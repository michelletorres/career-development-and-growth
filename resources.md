# ❤️ Curated list
Michelle's curated list of articles, videos and books around leadership in tech

## Management

- The Manager's Path: A Guide for Tech Leaders Navigating Growth and Change, by Camille Fournier
- High Output Management, by Andrew Grove
- The First 90 Days, Updated and Expanded: Proven Strategies for Getting Up to Speed Faster and Smarter

## Leadership

- Accelerate: Building and Scaling High Performing Technology Organizations

## Feedback

- Crucial Conversations: Tools for Talking When Stakes Are High, by Kerry Patterson
- Radical Candor, by Kim Scott



# Random list
Collection of material shared from multiple parties

## Articles

Wants, needs, and chasm-crossing, by @apenwarr
https://apenwarr.ca/log/20211024

Ratings as Incentives, by Kent Beck
https://geekincentives.substack.com/p/ratings-as-incentives?s=r

Why The Status Quo Is So Hard To Change In Engineering Teams, by Antoine Boulanger
https://www.okayhq.com/blog/status-quo-is-so-hard-to-change-in-engineering-teams

How product engineering teams avoid dependencies - the independent executor model, by Jade Rubick
https://www.rubick.com/independent-executor-model/

How to Influence Attitudes on your Team for Better Results, by Tim Raynolds
https://www.biodigitaljazz.tech/p/how-to-influence-attitudes-on-your?s=r

OOPS writeups, by Lorin Hochstein
https://surfingcomplexity.blog/2021/11/21/oops-writeups/
Optimize local dev environments for better onboarding, by James Turnbull
https://github.com/readme/guides/developer-onboarding

Great engineering teams focus on milestones instead of projects, by Jade Rubick
https://www.rubick.com/milestones-not-projects/

The Decision-Making Pendulum, by Candost Dagdeviren
https://candost.blog/the-decision-making-pendulum/

Managers should ask for feedback, by CJ Cenizal
https://www.cenizal.com/getting-feedback-from-direct-reports/

Hunting Tech Debt via Org Charts, by Marianne Bellotti
https://bellmar.medium.com/hunting-tech-debt-via-org-charts-92df0b253145

Cost of Attrition, by Benji Weber
https://benjiweber.co.uk/blog/2022/01/12/cost-of-attrition/

Working with Product Managers: Advice from PMs, by Gergely Orosz
https://newsletter.pragmaticengineer.com/p/working-with-product-managers-advice-from-pms?s=r

How to Write a Strategy Statement Your Team Will Actually Remember, by Nobl Academy
https://academy.nobl.io/how-to-write-a-strategy-your-team-will-remember/

Prioritization, multiple work streams, unplanned work. Oh my!, by Leeor Engel
https://leeorengel.medium.com/prioritization-multiple-work-streams-unplanned-work-oh-my-b0adf59404a4

5 Questions Every Manager Needs to Ask Their Direct Reports, by Susan Peppercorn
https://hbr.org/2022/01/5-questions-every-manager-needs-to-ask-their-direct-reports

Conducting a Successful Onboarding Plan and Onboarding Process, by Naomi Kriger
https://medium.com/nerd-for-tech/conducting-a-successful-onboarding-plan-and-onboarding-process-6ec1b01ec2ae
Momentum is Magic, by Evan Meagher
https://evanm.website/2022/02/momentum/

A Neuroscience-Backed Approach to Team Building, by Darryn King
https://almanac.io/async-review/a-neuroscience-backed-approach-to-team-building

Engineering Org Structures— The QRF Team Model, by Joseph Gefroh
https://betterprogramming.pub/engineering-org-structures-the-qrf-team-model-7b92031db33c

Desperation-induced Focus, by Ravi Gupta
https://rkg.blog/desperation-induced-focus.php

The mindset that kills product thinking, by Jeff Patton
https://www.jpattonassociates.com/mindset-that-kills-product-thinking/

The superpowers of Hybrid Profiles, by Florian Fesseler
https://medium.com/shipup-blog/the-superpowers-of-hybrid-profiles-7f1c71932913

Work estimates must account for friction, by Karl Wiegers
https://stackoverflow.blog/2022/02/14/work-estimates-must-account-for-friction/

How to scale a unicorn-building engineering team (and stay sane), by Gad Salner
https://medium.com/meliopayments/how-to-scale-a-unicorn-building-engineering-team-and-stay-sane-40af8ac7e3db

Your Startup’s Management Training Probably Sucks — Here’s How to Make it Better, by 1RReview
https://review.firstround.com/your-startups-management-training-probably-sucks-heres-how-to-make-it-better

7 Mental Models For Great Engineering Leadership, by Eiso Kant
https://athenian.com/blog/7-mental-models-for-great-engineering-leadership

Technical Debt’s Hidden Cousin: Dark Debt, by Russ Miles
https://esynergy.co.uk/technical-debts-hidden-cousin-dark-debt/

Software Engineering Leadership is not (Only) About Coding Well, by Guy Gadon
https://gadon.medium.com/software-engineering-leadership-is-not-only-about-coding-well-1d5d1e43029

How organisations are changing, by Swardley
https://swardley.medium.com/how-organisations-are-changing-cf80f3e2300


## Books

The Hard Thing About Hard Things: Building a Business When There Are No Easy Answers, by Ben Horowitz

### New Managers
The First 90 Days, Updated and Expanded: Proven Strategies for Getting Up to Speed Faster and Smarter

### Recognition of Team Members
Thanks for the Feedback: The Science and Art of Receiving Feedback Well

### Time Management
Make Time: How to Focus on What Matters Every Day

The Surprising Science of Meetings: How You Can Lead Your Team to Peak Performance

### Results
The 4 Disciplines of Execution: Achieving Your Wildly Important Goals

Getting Things Done: The Art of Stress-Free Productivity

Atomic Habits: An Easy & Proven Way to Build Good Habits & Break Bad Ones

The Power of Habit: Why We Do What We Do in Life and Business

Failing Forward

### Diversity Inclusion & Belonging
Wisdom at Work

The Culture Map: Breaking Through the Invisible Boundaries of Global Business

### Coaching
Multipliers

Challenging Coaching: Going Beyond Traditional Coaching to Face the FACTS

The Coaching Habit: Say Less, Ask More & Change the Way You Lead Forever

### Leadership
Who Moved My Cheese?: An A-Mazing Way to Deal with Change in Your Work and in Your Life

Dare to Lead: Brave Work. Tough Conversations. Whole Hearts.

Turn the Ship Around!: A True Story of Turning Followers into Leaders

The 21 Irrefutable Laws of Leadership: Follow Them and People Will Follow You
