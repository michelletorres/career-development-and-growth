# Manager Challenge progam

For self-tracking progress of the [Manager Challenge](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-challenge) program

## Epics and issues

https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/58

- [Week 1 - October Manager Challenge](gitlab-com/people-group/learning-development&59)
    - [Day 1 - Getting to Know Yourself and Team Members](gitlab-com/people-group/learning-development/challenges#68) 
    - [Day 2 - Building a Psychologically Safe Environment](gitlab-com/people-group/learning-development/challenges#69)
    - [Day 3 - Building High Performing Teams](gitlab-com/people-group/learning-development/challenges#70)
    - [Day 4 - Live Learning on Building High Performing Teams](gitlab-com/people-group/learning-development/challenges#71)
    - [Day 5 - Catch-Up Day and Self-Reflection](gitlab-com/people-group/learning-development/challenges#72)
- [Week 2 - October Manager Challenge](gitlab-com/people-group/learning-development&60)
    - [Day 6 - Running an Effective 1:1 Meeting with Feedback](gitlab-com/people-group/learning-development/challenges#73)
    - [Day 7 - Live Learning Session on Giving Feedback](gitlab-com/people-group/learning-development/challenges#74)
    - [Day 8 - Coaching & Performance](gitlab-com/people-group/learning-development/challenges#75)
    - [Day 9 - Live Learning Session on Coaching & Performance Management](gitlab-com/people-group/learning-development/challenges#76)
    - [Day 10 - Final Evaluation and Post Challenge Activity](gitlab-com/people-group/learning-development/challenges#77)

