# Managers & Mental Health

2022-04-14 - Session with Dr. Jody Adewale by Modern Health

[Recording](https://joinmodernhealth.zoom.us/rec/share/nlyT-TqxqA1uaSs3zK_x2DTXjI5ip0WJy5sqgU8KyZLb7szRjhJm5rqxZjT8BeIz.KEwgk0LXY0fP4zUt) (available only for team members)

---

We usually pay a lot of attention to physical health, but not to mental health.


## Most common mental health issues in the workplace

75% percent of people in the workplace report moderate to high levels of stress

As a manager, you will probably see one of these three:
- Depression
- Anxiety
- Stress

On multiple occasions, this is due to their relationship with their manager.



## Stress and the relation with performance

![stress level and performance chart](img/stress-performance.png)

Optimum stress - Stress becomes a motivator or mobilizer to engage in certain activities.

When we cannot act on that stress or turn it into action, we end up in anxiety, panic, anger, or even burnout.

💡 As a manager, continuously question and evaluate how is the stress of the team


## Burnout

Burnout isn't a clinical diagnosis. It is a symptom of multiple diagnoses.

Performance drops, and it manifests in different areas:
- Feelings
- Thoughts - Reactions
- Actions - Behaviors
- Body - Physical reactions

Some signs could be:
- Physical and/or emotional exhaustion. Mental and emotional energy takes a lot of energy. With physical exhaustion, sleeps works, but not with emotional exhaustion. You wake up, and you still feel exhausted.
- Cynicism and/or detachment. Overly cynical. They bring a lot of negativity to work. Detachment or, in a more severe issue, can be dissociation.
- Feelings of ineffectiveness and/or lack of accomplishment. The example of how elephants are trained for the circus. The internalization of that sense of helplessness that there is nothing I can do to improve my situation.

## Modeling behaviors

It all starts with Observational Learning. You are not your employee's parent, but you are in a position of authority, and they look at you to see what is ok and what is not. 

Take time off, and encourage team members to do it.

In modeling that behavior, you start to model psychological safety.

## Psychological safety

> Psychological safety is a belief that one will not be punished or humiliated for speaking up with ideas, questions, concerns, or mistakes
> - Amy Edmondson; Harvard Business School Professor

🎗 Is there anything that I'm doing right now and did in the past that contributes to psychological safety in the workplace?

What creates psychological safety:
- Own up to your mistakes and use a growth mindset to discuss key learnings
- Seek out divergent thinking
- Approach from a place of curiosity instead of blame


Good leaders have people around them who are not afraid of them.

Ideas:
- Weekly check-ins: From 1-10, how are you doing? Someone from 8-10, please share what is going well. For people in the lower ranges, ask them if there is something we can do to make things better.

## Help team members find a new balance

Some of us don't have that skill to balance things; others are born knowing how to balance things really well.

Communicate to your team frequently high-value vs. low-value tasks. Some people struggle to prioritize, especially when the ask is coming from their manager.

Clearly communicate expectations. We do it usually when we hire and slowly stop doing it as time goes. 

## Leader vs. Boss

We will need both, but be conscious and mindful of when and how we do "Push and tell" and "Pull and show"

## Help team members establish boundaries

Boundaries is an umbrella term
- emotional - I can feel what I want, and you can feel what you want
- intellectual - I should be able to think how I want, and you do the same
- physical
- time - I shouldn't show up to this meeting late and disrespect your time boundaries
- financial - We shouldn't be taking money from one another when it is inappropriate


💡 How are team members communicating their boundaries, and are we respecting them?

Mental health and stress level is often impacted when boundaries are not respected.

## Offer support

To do:
- Be mindful of the stigma around mental health
- Pay attention to body language
- Don't rush conversations


Avoid:
- give specific mental health advice
- dismiss their concerns
- become the only source of support


## Attribution theory

![atrribution theory](img/attribution-theory.png)

Frtiz Heider suggested we tend to give simple explanations for someone's behavior, often by crediting either the situation or the person's disposition.

