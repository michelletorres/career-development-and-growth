# Effective 1:1s

Conversations that drive engagement and performance

1:1 allow us to connect different areas from the different skills we saw in the previous sessions.

## Purpose

Often is weekly or every other week, from 30-60 mins, and focus on the person and not just updates.

Looking for high performance, and we know it depends on high engagement.

How do you know? What do you see? What kind of behaviors are there when employees are highly engaged?
- Challenge the status quo, ask questions, mainly why's
- Constantly contribute to team goals
- Collaborate with peers and others

Super interesting to read others' answers:...
- frequent contributions to epics, issues, collaborates with teams and customers
- In video calls - Animated physically, talk with excitement
- Volunteering, collaborating, signing up for projects, their results data, training done - etc.
- person is active is not active within their own tasks, but also reviewing, helping others, being proactive in terms of seeing and addressing problematic areas
- I notice it when they feel they are part of the team -- by the quality of their contributions, helping others, engaging with peers/counterparts in design problems
- engaged in MRs, issues, discussions. Helping others inside and outside team. Bring solutions as well problems
- Work wise - blend of working through tasks and issues effectively with making improvements and suggesting new ways to solve a problem

> "A combination of commitment to org and eager willingness to help colleagues. A heightened emotional connection that influences you to exert greater discretionary effort."

discretionary effort - it usually aligns with going above and beyond, the extra things, 

Employee engagement cannot be demanded, but it can be engineered. We can engineer it, and work with individuals to make it happen

## Satisfy five brain cravings

- Progress
- Meaning
- Autonomy
- Social inclusion
- Certainty

### Progress

Progress is very important. Dopamine is released when we complete things, like when getting to the finish line, when marking check boxes, or when a gamer made to a certain level. 

From an investigation, it was discovered people's satisfaction came from the work in between to achieve a goal than the goal itself.

To satisfy progress:
- start 1:1s with small wins
- stuck? help use task boxing
- give ongoing skill-based feedback
- create a development plan + review in each 1-1. Set small development goals

Our brain chunks information to reduce the cognitive load. We did a test memorizing a big number. In my case, I split the number into chunks of 4, 1, 2, and 3. It is curious that I did it based on what it makes sense on "sound" and nothing else.

When we find people is not yet starting a project, use progress coaching
- what does success look like, obstacles, and options?
- how will you know you are done?
- how can you break up the project into phases?
- how long will each one take?
- what's the smallest first step?
- when should we check in?

Exercise time!

> 4 minutes of coaching. GO!
> Coach: ask, "What do you want to get better at?"
> 
> 1. Deblur: What does _____ mean to you?
> 2. Success? Obstacles? Options?
> 3. Task-box: Next smallest step?
> 
> Time's up! 2-min debrief. What did the coach do well? What to improve?
> 
> Get ready to switch roles...

### Meaning

Our brain is a linking machine.

In the example of people building a cathedral, the mission was unclear to all.

What is the mission + How does my work contribute?

Use impact statements regularly.

This matter because ...

Exercise time!...

> Share: For one of your reports, how does their work contribute to the mission (as you see it)?

To satisfy meaning:
- Ask what they find most meaningful
- Link their work to the mission
- 

### Autonomy

Choice = Power

To reduce: offer training + guidance; set checkpoints.

To increase: ask for input; give choice + ownership

To help find autonomy level, ask yourself, what is the team member the CEO of?

I found this topic somewhat conflicting with our manager of one value at GitLab. Also, the topic of too much autonomy, I can see and imagine examples in other companies and industries, but not sure I can relate to how we work at GitLab.

Check in on autonomy levels
Explore ways to increase/
Give ownership / CEOship

### Social inclusion

Social urge = survival urge

Social pain is similar to physical pain (from the Novembre experiment)

To satisfy social inclusion:
- small talk counts: What can you ask about them / share about yourself?
- build networks: who do they go to for support? Who would they have more contact with? how do we connect individuals?
- team dynamics: they need more/less? What do they need to feel connected?

Interesting to hear how others build that sense of belonging and social inclusion:
- social slack channel with questions
- checkins on 1:1s

### Certainty

Research about prospect-refuge about landscape images that the majority select. 

To satisfy certainty:
- create rituals:  standup meetings, 1:1s, things that they know is going to happen consistently
- be consistent: do not cancel 1:1s; reschedule, but make sure not to cancel
- take notes and follow up: is important so you can keep progress tracking from the previous 1:1s
- clarify expectations (role, goals, MITs): talk about buckets, most important things, etc. - all that we discussed in productivity & prioritization

## Evaluation

Evaluate where your team members are. Score 1-10, where 10 is highly satisfied with 

| Team member | Progress | Meaning | Autonomy | Social inclusion | Certainty |
| ------ | ------ | ------ | ------ | ------ | ------ |
|  |  |  |  |  |  |

Use this to drive the conversation in your next 1:1s

Think about their thinking. What you have in mind may differ from what they think. Make sure to check on CAMPS (certainty, autonomy, meaning, progress, social inclusion) in the 1:1s

Other material:
- [Sample agenda](https://docs.google.com/document/d/19sPLkloSmHJqJ_Jz1wS_VmyjgXtPuRgGtWNboRL_UO0/edit)
- Reference: https://gitlab.com/gitlab-com/people-group/learning-development/training-curriculum/-/issues/57 _only visible to GitLab team members as it may contain intellectual property of LifeLabs_
