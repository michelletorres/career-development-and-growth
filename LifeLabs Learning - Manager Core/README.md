# Manager training

This training focuses on 4 core skills for great managers. The skill to bring out the best in you and your team.

1. [Coaching skills](/LifeLabs%20Learning%20-%20Manager%20Core/coaching-skills.md)
1. [Feedback skills](/LifeLabs%20Learning%20-%20Manager%20Core/feedback-skills.md)
1. [Productivity skills](/LifeLabs%20Learning%20-%20Manager%20Core/productivity-prioritization.md)
1. Effective 1-1s

---

[Program details](https://gitlab.com/gitlab-com/people-group/learning-development/training-curriculum/-/issues/60) - _Only visible to GitLab team members as it may contain intellectual property of LifeLabs._
