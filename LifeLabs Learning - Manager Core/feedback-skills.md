# Feedback skills

## Make it receivable

Confirm the person is prepared to receive feedback

- Can we do a quick check-in on X?
- Do you have time to talk about X?
- I ahve some feedback on X. Can I share it with you?

Super important to talk about the action and not the person

| No | Yes |
| ------ | ------ |
| You are too sloppy | Your email is too sloppy |
| You aren't realistic | Your proposal isn't realistic |
| You were rude | Your tone is unfriendly |
| You are awesome | Your summary is awesome |


## Make it actionable

Use the LifeLabs Playing Cards Method™️ and focus on specifics.

♣️ Club: Blurry critique  
♥ Heart: Blurry praise  
♠️ Spade: Specific critique  
♦ Diamond: Specific praise  
 
Don't sugar coat, don't pad. State critiques clearly.

Specify the behavior and add an impact statement

"I noticed you gathered the data into a one page and presented a list of actions to take" ➕ "It will make it much easier to make a efficient decision"


## Make it balanced

Notice what people do well and strenghen their strengths!

A balacne dialogue goes:
- Ask - use questions to get a micro-yes and get buy-in
- Tell - the formular from actionable: behavior + impact
- Ask - Confirm the message is received and open the conversation

Questions that can be used are:
- What are your thoughts?
- How do you see it?
- How can you do it differently in the future?

## Prep

Prepare for the conversation.

| 1. Micro-Yes | 2. Behavior | 3. Impact statement | 4. Question |
| --- | --- | --- | --- |
| **Get buy-in. Reduce mystery / allow prep** | **Focus on behavior, not person. Deblur** | **Why does this matter? Who is affected by it?** | **Check how they see it. Agree to an action plan** |
| Do you have 10 minutes to talk about your last email to Jill? | I noticed you replied to her email three days after she sent it. | I mention it because she can’t move forward without your reply, so it might delay her team. | What do you think our process should be moving forward? |
| Can I share some thoughts with you about that meeting? | During the meeting you announced to everyone that there is a delayed schedule before letting me know. | I bring it up because we looked uncoordinated in front of our clients, which can impact their trust. | How do you see it? Can we agree to…? |

---

Reference: https://gitlab.com/gitlab-com/people-group/learning-development/training-curriculum/-/issues/55 _only visible to GitLab team members as it may contain intellectual property of LifeLabs_
