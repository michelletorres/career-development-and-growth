# Coaching skills

In average, a conversation have 2 questions per 15 mins. A great manager conversation will have 10 questions per 15 mins.

## Why ask questions

We ask questions to:
- make sure we understood
- help coachee to clarify their thinking
- empower them to take ownership

The difficult part is to move to make questions mode as your default mode.

## Which questions

Only authentic questions!

Scenario: A team member wants to start a new project. Your instinct is that they are too busy.

⚠️ Dont's:
- aren't you a little busy right now?


## Coaching tools

### 1. Playbacks and split tracks

Use clarification questions and ask them regularly.

Just to make suire I understand, you said X. Did I get it right?

Ok, I hear a few things: X and Y. Which one should we focus on first?

### 2. Open vs Closed

Closed:
- Do
- Are
- Is
- Have
- Can
- Would

Open:
- What
- How
- When
- Who
- Which
- Why

### 3. SOON funnel™️

Use clarification questions and ask them regularly:
- Playback: Okay, so you are wondering about X. Did I get it right?  
- Split-track: It sounds like two issues, x and y. Is that right? Which should we focus on first?
- You said X. What does X mean to you? (e.g., prioritization, ownership, etc.)
- Can you give me an example?
- Can you walk me through your thinking on that?
- On a scale from 1-10, what would you say your current level of Z is? (e.g., motivation)
- What emotions does it bring up? What is important to you about it?

We know questions matter, but which questions count most? To simplify complexity, you can boil the coaching process down to four fundamental categories called the SOON funnel: questions around success, obstacles, options and next steps.

#### Success
- What would success look like (for this issue)?
- What would tell you that you’ve reached your goal?
- How should we best go about thinking about this?
- What would be the benefit?
- What do we hope to achieve in the next X minutes?
- What is the purpose of this meeting?
- What decisions need to be made? What are your decision criteria?

#### Obstacles
- What are the obstacles?
- What is holding you back?
- What might get in the way?
- What concerns you most about it? What are your hesitations?
- What might be the unintended consequences?
- What are the advantages or disadvantages of starting sooner/later?
- Whose buy-in do you need to get?

#### Options
- What are the options? What else?
- What have you tried so far?
- Would you like to brainstorm on this idea?
- When does this problem occur? When doesn’t it occur?
- What’s working well?
- What other angles can you think of? Other possibilities?
- How else could a person handle this?
- If you could do anything, what would you do?
- Who can help with this? Who else needs to be involved?
- If you could do it over, what would you do differently?

#### Next Steps
- What are your next steps?
- What’s the first small step? An even smaller step?
- What needs to be done to get this moving?   	
- How committed are you to doing this? (1-10 scale) What would  increase this score by one point? 
- What will influence the timing? When will you start?
- What is the back-up plan?
- What can we learn that we can use in the future?
- Was this a good use of our time?
- What can we repeat/do differently moving forward?

---

Reference: https://gitlab.com/gitlab-com/people-group/learning-development/training-curriculum/-/issues/54 _only visible to GitLab team members as it may contain intellectual property of LifeLabs_
