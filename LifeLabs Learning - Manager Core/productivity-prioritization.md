# Productivity & Prioritization

Top 4 challenges across companies:
- chronemics - time awareness
  - how long something can take
  - how long something was taking in the past
- prioritization: what gives the greatest value
- organization: efficient workflow
- focus: working without disruptions

## Chronemics

Time integrity:
- in our last 10mins ...
- should we extend by X mins?
- what's your estimate?
- was the estimate correct?

Audit:
- where is my time going?
- how much planned vs reactive time do I have?

Cultivate time integrity: no being late, finishing on time

Clean up blur words like two seconds, asap, sometime etc


## Prioritization

### MIT method
Most important things (MIT) method

### Quadrant method

Urgent = has time pressure
Important = adds long-term value to the org or team

|  | Urgent | Not urgent |
| --- | --- | --- |
| Important | Q1: fires | Q2: investment | 
| Less important | Q3: deception | Q4: recharge |

Q2: calendar-block to focus on this  

Q3: Is it the MIT?  
- Am I the best one to do it or can I delegate?  
- Is it already good enough?  
- Here are things that do not matter, pseudo-progress, emails, quick questions  

Q4: Is it urgent? Is it Important?  
We can count breaks (or micro-breaks here)

### Bucket method

Headlines for the quarter + activies for the week

- Decide which 3 things are the most important for this quarter + other bucket  
- categorize tasks for this weeks into those buckets
- reduces overwhelm, help with bucket trade-offs

## Organization

Beware of the Zeigarnik Effect. Open loops take up valuable brain space.

To close the loops the usage of To-Do list, calendar and notes helps.

Build a culture to close loops:
- who will own this?
- who is capturing this?
- how should we follow up on this?

## Focus

44% of interuptions are self-interuptions

context switch causes cognitive lag

What helps: pomodoro technique, kanban, If-then cues (if it is morning, I will check my email etc)

If-then when getting requests from others:
- if focused, then airplane mode
- if request, then ask for deadline

---

Reference: https://gitlab.com/gitlab-com/people-group/learning-development/training-curriculum/-/issues/56 _only visible to GitLab team members as it may contain intellectual property of LifeLabs_
